<?php 

    define('DB_HOST', 'database');
    define('DB_NAME', 'occazz');
    define('DB_USER', 'root');
    define('DB_PASS', '');     

    define('DS', DIRECTORY_SEPARATOR);

    define('APP_ROOT', dirname (__FILE__) . DS);
    define('PATH_PAGES', APP_ROOT. 'occazz' . DS . 'pages' .DS);
    define('PATH_PARTIALS', PATH_PAGES. '_partials' .DS);

    define('URL_ROOT', $_SERVER['REQUEST_SCHEME']. '://' .$_SERVER['HTTP_HOST']);

$dsn = 'mysql:dbname=' . DB_NAME . ';host=' . DB_HOST;

$pdo_opt = [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
];

try {

    $pdo_cnx = new PDO ($dsn, DB_USER, DB_PASS, $pdo_opt);

}

catch( PDOException $e) {
    http_response_code(500);
    die('Une erreur s\'est produite... ');
}

// Démarrage

    require_once APP_ROOT . 'occazz' . DS . 'functions.php';
    require_once APP_ROOT . 'occazz' . DS . 'router.php';

?>