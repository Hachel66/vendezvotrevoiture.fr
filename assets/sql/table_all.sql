SELECT v.`id`, v.`title`, v.`description`, v.`price`, v.`kilometers`, md.`name` AS model, ma.`name` AS brand , co.`name` AS color, cy.`value` AS volume, ca.`name` AS fuel 
                    FROM `Voitures` AS v 
                    JOIN `Modeles` AS md ON v.`model_id` = md.`id`
                    JOIN `Marques` AS ma ON md.`brand_id` = ma.`id`
                    JOIN `Couleurs` AS co ON v.`color_id` = co.`id`
                    JOIN `Moteurs` AS mt ON v.`motor_id` = mt.`id`
                    JOIN `Cylindrees` AS cy ON mt.`cylinder_id` = cy.`id`
                    JOIN `Carburant` AS ca ON mt.`carburant_id` = ca.`id`
                    ORDER BY v.`price` DESC;