<?php

    // -------------------------------------------

$qAssoc = [];

$q = 'SELECT v.`id`, v.`title`, v.`description`, v.`price`, v.`kilometers`, md.`name` AS model, ma.`name` AS brand , co.`name` AS color, cy.`value` AS volume, ca.`name` AS fuel 
FROM `Voitures` AS v JOIN `Modeles` AS md ON v.`model_id` = md.`id`';

if ( !empty( $_POST['model'] ) ) {

    $q .= ' AND md.`value` = :model';
    $qAssoc['model'] = $_POST['model'];

};

$q .= ' JOIN `Marques` AS ma ON md.`brand_id` = ma.`id`';

if ( !empty( $_POST['brand'] ) ) {

    $q .= ' AND ma.`id` = :brand';
    $qAssoc['brand'] = $_POST['brand'];

};

$q .= ' JOIN `Couleurs` AS co ON v.`color_id` = co.`id`';

if ( !empty( $_POST['color'] ) ) {

    $q .= ' AND co.`id` = :color';
    $qAssoc['color'] = $_POST['color'];

};

$q .= ' JOIN `Moteurs` AS mt ON v.`motor_id` = mt.`id` JOIN `Cylindrees` AS cy ON mt.`cylinder_id` = cy.`id`';

if ( !empty( $_POST['cylinder'] ) ) {

    $q .= ' AND cy.`id` = :cylinder';
    $qAssoc['cylinder'] = $_POST['cylinder'];

};

$q .= ' JOIN `Carburant` AS ca ON mt.`carburant_id` = ca.`id`';

if ( !empty( $_POST['fuel'] ) ) {

    $q .= ' AND ca.`id` = :fuel';
    $qAssoc['fuel'] = $_POST['fuel'];

};

$qpart = [];

if ( !empty( $_POST['kilometers'] ) ) {

    array_push($qpart, 'v.`kilometers` <= :kilometers');
    $qAssoc['kilometers'] = $_POST['kilometers'];

};

if ( !empty( $_POST['pricemax'] ) ) {

    array_push($qpart, 'v.`price` <= :pricemax');
    $qAssoc['pricemax'] = $_POST['pricemax'];

};

if ( !empty( $_POST['pricemin'] ) ) {

    array_push($qpart, 'v.`price` >= :pricemin');
    $qAssoc['pricemin'] = $_POST['pricemin'];

};

if ($qpart != null) {

    $q .= ' WHERE ' . join(' AND ', $qpart);

};

$q .= ' ORDER BY v.`price` DESC';

var_dump($q);

// -------------------------------------------

    $arr_cars = [];

    $stmt = $pdo_cnx->prepare( $q );

    // var_dump($pdo_cnx->errorInfo());

    if ( !empty($qAssoc) ) {
        $stmt->execute($qAssoc);
    }

    if($stmt) {

        // 4.1 - Récupération du nombre de résultats de la requête
        $result_count =  $stmt->rowCount();

        // 4.2 - S'il y a des résutats on les traite
        if( $result_count > 0 ) {
            
            while( $row = $stmt->fetch() ) {
                array_push( $arr_cars, $row );
            }
        }
    
    }

    // Si la préparation échoue
    else {
        var_dump( $pdo_cnx->errorInfo() );
    }

?>
