
<?php

/*
Liste véhicules
*/

$html_title = 'Véhicules';

require_once PATH_PARTIALS. 'header.inc.php' ;

?> 

<main>

    <section id="section-results">

        <h1>Nos Véhicules</h1>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis mollitia ex, voluptatibus deleniti earum ducimus vero, voluptatem unde saepe eaque consectetur laboriosam, repudiandae nemo? Quo aspernatur sapiente voluptate natus reiciendis.</p>
        <br>

        <?php 

        require_once PATH_PAGES .DS. 'cars' .DS. 'cars-list.code.php';
        
            $result_count = count($arr_cars);

            if ($result_count > 0) {

                echo 'Il y a '. $result_count . ' résultats.<br>';

                //var_dump($arr_cars);
                
                ?>

                <ol>

                <?php foreach ($arr_cars as $car) :?>

                    <li class="card">
                        <a href="<?php _url('/nos-vehicules/'. $car['id'])?>">

                        <h2><?php _e($car['title'])?></h2>
                        <p><?php _e($car['description'])?></p>
                        <div><p><b>Modèle: </b><?php _e($car['brand'])?></p>
                        <p><b>Couleur: </b><?php _e($car['color'])?></p>
                        <p><b>Kilométrage: </b><?php _e($car['kilometers'])?></p>
                        <p><b>Motorisation: </b><?php _e($car['fuel'])?> <?php _e($car['volume'])?>L</p>
                        <p><b>Prix: </b><?php _e($car['price'])?>€</p></div>
                        </a>
                    </li>

                <?php endforeach; ?>

                </ol>
            <?php }else{

                echo 'Aucun résultat !';

            }     
           
        ?>

    </section>

    <section id="section-search">
    <form method="POST">

        <div>
            <label for="brand">Marque :</label> <br>

            <select name="brand" id="select-brand">

                <option value="">-- Marque --</option>

                <?php 

                    $q = file_get_contents('assets/sql/table_brands.sql');
                    $stmt = $pdo_cnx->query( $q );

                    while( $data = $stmt->fetch() ) {
                        echo '<option value="' . $data['id'] . '">' . $data['name'] . '</option>'; 
                    };             

                ?>

            </select>

            <!-- <select name="" id=""></select> -->

        </div>

        <div>
            <label for="color">Couleur :</label> <br>

            <select name="color" id="select-color">
                <option value="">-- Couleur --</option>

                <?php 

                    $q = file_get_contents("assets/sql/table_colors.sql");
                    $stmt = $pdo_cnx->query( $q );

                    while( $data = $stmt->fetch() ) {
                        echo '<option value="' . $data['id'] . '">' . $data['name'] . '</option>'; 
                    };           
                    
                ?>

            </select>

        </div>

        <div>
            <label for="fuel">Motorisation :</label> <br>

            <select name="fuel" id="select-color">
                <option value="">-- Motorisation --</option>
                
                <?php 

                    $q = file_get_contents("assets/sql/table_fuel.sql");
                    $stmt = $pdo_cnx->query( $q );

                    while( $data = $stmt->fetch() ) {
                        echo '<option value="' . $data['id'] . '">' . $data['name'] . '</option>'; 
                    };           

                ?>
            </select>

            <!-- <select name="" id=""></select> -->

        </div>
            
        <div>
            <label for="km">Kilométrage :</label> <br>

            <input placeholder="0" min="0" name="kilometers" type="number">

        </div>

        <div>
            <label for="km">Prix Min. (en €) :</label> <br>

            <input id="input-pricemin" name="pricemin" placeholder="0" min="0" type="number">

        </div>

        <div>
            <label for="km">Prix Max. (en €) :</label> <br>

            <input id="input-pricemax" name="pricemax" placeholder="0" min="0" type="number">

        </div>

        <div><input type="submit" value="Vroooom !"></div>

    </form>
    </section>

</main>

<?php

require_once PATH_PARTIALS. 'footer.inc.php' ; 

