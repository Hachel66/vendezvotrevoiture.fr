<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title><?php _e($html_title) ?> - Vendezvotrevoiture.fr</title>

    <link rel="stylesheet" href="<?php _asset('css/style.css')?>">

</head>

<body>

    <header> 
    
    <img height="50px" width="50px" src="/assets/img/logo.jpg"> 
    
    <span>Vendezvotrevoiture.fr</span> 
    
    <nav>
        <a href="<?php _url('') ?>">Accueil</a> - 
        <a href="<?php _url('/nos-vehicules') ?>">Nos véhicules</a> - 
        <a href="<?php _url('/contact') ?>">Contact</a>
    </nav>

    </header>