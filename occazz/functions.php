<?php 

/*
Fontions pour faciliter le code de tout le projet
*/

// Ici les fonctions commençant par _ "écho" du contenu.

/** 
* Echo un contenu
* @param string $content Contenu à être "écho"
*
* @return void
*/

function _e(string $content): void 
{
    echo $content;
}

/** 
* Echo une URL absolue à partir de la racine
*
* @param string $url_part URL relative à traiter
*
* @return void
*/

function _url(string $url_part): void 
{
    echo URL_ROOT . $url_part;
}

/** 
* Echo de l'URL d'un asset donné
* @param string $asset_path Chemin de l'asset sans le "/" devant
*
* @return void
*/

function _asset(string $asset_part): void 
{
    echo URL_ROOT . '/assets/' . $asset_part;
}

?>